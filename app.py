from flask import Flask, render_template, url_for, request,redirect
from script.deck import *
import sqlite3

app = Flask(__name__)

@app.route('/')
@app.route('/home')
def home_page():
    data = listing_all_deck()
    return render_template('index.html', data=data, bite= bite(5))

def test(n):
    return str(n) + " yes"

@app.route('/success/<name>')
def success(name):
   return 'welcome %s' % name

@app.route('/login',methods = ['POST', 'GET'])
def login():
   if request.method == 'POST':
      user = request.form['nm']
      return redirect(url_for('success',name = user))
   else:
      user = request.args.get('nm')
      return redirect(url_for('success',name = user))

if __name__ == '__main__':
    app.run()